require './app/loggable'
require './app/iso8583'
require './app/iso8583_core2'
require './app/worker'
require './app/controller'
require './app/tester'

set :bind, "0.0.0.0"

$login = false   # if the server restart, we arent sign on, so we need to sign on
$echo_duration = '50s'   # interval between echo if the echo is success, default 180 seconds / 3 minutes
$re_request = 5   # times to rerequest when the request is failed in seconds

configure do
    scheduler = Rufus::Scheduler.new
    set :scheduler, scheduler
end

settings.scheduler.every($echo_duration) do
    echoing = EbillingGateway::Controller.new
    echoing.do_echo
end

settings.scheduler.cron '0 15 * * *' do
    sleep 2
    puts "#{(Time.now.utc + 7 * 60 * 60).strftime "%Y-%m-%d %H:%M:%S.%L"}"
    echoing = EbillingGateway::Controller.new
    echoing.do_echo
end