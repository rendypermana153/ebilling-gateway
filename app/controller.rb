module EbillingGateway
    
    class Controller
        include Loggable

        def do_signon
            identifier = get_identifier
            logger "New client. Request no. #{identifier}"
            logger "Signin started.", identifier
            if $login == false
                signon = Worker::BackgroundWorker.new
                logger "Signing in...", identifier
                signon.do_signon(identifier)
            else
                logger "Signin can't be performed: still signed in.", identifier
                { status: "Failed", message: "Signin can't be performed: still signed in." }.to_json
            end
        end

        def do_echo
            identifier = get_identifier
            logger "New client. Request no. #{identifier}"
            logger "Echo started.", identifier
            if $login == true
                echoing = Worker::BackgroundWorker.new
                logger "Echoing...", identifier
                echoing.do_echo(identifier)
            else
                logger "Echo can't be performed, not signed in.", identifier
                logger "Signin started.", identifier
                begin
                    signon = Worker::BackgroundWorker.new
                    logger "Signing in...", identifier
                    signon.do_signon(identifier)
                rescue => e
                    logger "Cannot connect to tcp-server: #{e}\n", identifier     
                end
            end
        end

        def do_signoff
            identifier = get_identifier
            logger "New client. Request no. #{identifier}"
            logger "Signoff started.", identifier
            if $login == true
                signoff = Worker::BackgroundWorker.new
                logger "Signing off...", identifier
                signoff.do_signoff(identifier)
            else
                logger "Signoff can't be performed: not signed in.", identifier
                { status: "Failed", message: "Signoff can't be performed: not signed in." }.to_json
            end
        end

        def do_financial_request_as_gateway(request)
            identifier = get_identifier
            logger "New client. Request no. #{identifier}"
            logger "Financial transaction request received.", identifier
            if $login == true
                fin_req = Worker::FinancialRequest.new
                logger "Sending request to DJP...", identifier
                fin_req.is_signed_in_as_gateway(request, identifier)
            else
                message = "Financial request can't be performed. Not signed in."
                logger message, identifier
                { status: 'Failed', code: 'YY', result: message }.to_json
            end
        end

        def change_log_status(request)
            logger "Change log status received."
            unless request['status'] == true ||  request['status'] == false
                logger "Error: Bad request"
                return { status: "Failed", message: "Bad request." }.to_json
            end
            $login = request['status']
            logger "Log status has been change to #{request['status']}"
            return { status: "OK", message: "#{request['status']}" }.to_json
        end
    end
end