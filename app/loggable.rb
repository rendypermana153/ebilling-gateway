module Loggable
    def get_identifier
        SecureRandom.hex(3)
    end
    def time_for_log
        "#{(Time.now.utc + 7 * 60 * 60).strftime "%Y-%m-%d %H:%M:%S.%L"}:"
    end

    def logger(message, identifier="system")
        File.open("/home/fintax-eb-19/Documents/log/iso8583/#{(Time.now.utc + 7 * 60 * 60).strftime "%Y-%m-%d"}.log", 'a') { |f| f.puts "#{time_for_log} #{message}" }
        puts "#{time_for_log} #{identifier} -- #{message}"
    end
end