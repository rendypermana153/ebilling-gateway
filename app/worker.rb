module Worker

    class DefaultWorker
        include Loggable

        def bit_11
            # declaring for bit-11, if bit-11 of the response is not the same as the request, it is an error
            rand(0..999999).to_s.rjust(6, "0")
        end

        def get_response_length(request_mti, request_3, iso_value)
            # CORE 1
            # if request_3.nil?
            #     response_length = iso_value.length + 2 if request_mti == "0800"
            # else
            #     return response_length = iso_value.length + 2 + 135 if request_3 == "300000"
            #     return response_length = iso_value.length + 2 + 15 if request_3 == "400100"
            #     return response_length = iso_value.length + 2 + 15 if request_3 == "400101"
            # end
            # CORE 2
            if request_3.nil?
                response_length = iso_value.length + 2 if request_mti == "0800"
            else
                return response_length = iso_value.length + 2 + 136 if request_3 == "300000"
                return response_length = iso_value.length + 2 + 109 if request_3 == "400200"
                return response_length = iso_value.length + 2 + 29  if request_3 == "400201"
            end
        end

        def encode_to_send(request, identifier)
            # CORE 1
            # iso_value = ISO8583::Encoder.new
            # iso_value.iso8583 = nil
            # response = nil
            # request.each { |keyz, valuez| iso_value.setBit(keyz, valuez) }

            # CORE 2
            iso_value = ISO8583Core2::Encoder.new
            iso_value.iso8583 = nil
            response = nil
            request.each { |keyz, valuez| iso_value.setBit(keyz, valuez) }
            iso_value.finalize

            logger "Req: #{iso_value.iso8583}", identifier
            
            # TCP connection no timeout
            # stream_sock = TCPSocket.new("10.245.194.200", 8292)
            # stream_sock = TCPSocket.new("10.240.67.18", 8081)
            stream_sock = TCPSocket.new("10.152.18.29", 8081)
            # stream_sock = TCPSocket.new("10.240.67.170", 8081)
            stream_sock.puts(iso_value.iso8583)
            begin
                response = stream_sock.read_nonblock(self.get_response_length(request["mti"], request["3"], iso_value.iso8583))
            rescue Errno::EAGAIN
                retry
            end
            stream_sock.close

            # TCP using gem net http client, timeout 10s
            # Net::TCPClient.connect(server: '0.0.0.0:8081') do |client|
            #     client.write(iso_value.iso8583)
            #     response = client.read(self.get_response_length(request["mti"], request["3"], iso_value.iso8583))
            # end

            # HTTPS connection
            # response = RestClient.post 'http://localhost:8080/v1/givemerequest', iso_value.iso8583.to_json, { content_type: :json, accept: :json }
            $login = false if response.nil?
            logger "Res: #{response}", identifier
            return response
        end

        def decode_the(response)
            # CORE 1
            # iso_value = ISO8583::Decoder.new
            # CORE 2
            iso_value = ISO8583Core2::Decoder.new

            iso_value.iso8583 = response
            message = iso_value.decode_this_message(iso_value.iso8583)
            return message
        end
    end

    class BackgroundWorker < DefaultWorker

        def do_signon(identifier)
            request = signon_request
            response = self.encode_to_send(request, identifier)
            message = self.decode_the(response)
            response_is_false(message, request) ? signon_failed(request, message, identifier) : signon_success(request, message, identifier)
        end

        def do_echo(identifier)
            request = echo_request
            response = self.encode_to_send(request, identifier)
            message = self.decode_the(response)
            response_is_false(message, request) ? echo_failed(request, message, identifier) : echo_success(request, message, identifier)
        end

        def do_signoff(identifier)
            request = signoff_request
            response = self.encode_to_send(request, identifier)
            message = self.decode_the(response)
            response_is_false(message, request) ? signoff_failed(request, message, identifier) : signoff_success(request, message, identifier)
        end

        private

        def signon_request
            {
                "mti" => "0800",
                "1" => true,
                "7" => nil,
                "11" => bit_11,
                "70" => "001"
            }
        end

        def echo_request
            {
                "mti" => "0800",
                "1" => true,
                "7" => nil,
                "11" => bit_11,
                "70" => "301"
            }
        end

        def signoff_request
            {
                "mti" => "0800",
                "1" => true,
                "7" => nil,
                "11" => bit_11,
                "70" => "002"
            }
        end

        def response_is_false(message, request)
            message["mti"] != "0810"        ||
            message["11"]  != request["11"] || 
            message["39"]  != "00"          ||
            message["70"]  != request["70"]
        end

        def signon_success(req, res, identifier)
            logger "Sign in success.", identifier
            $login = true
            return { status: "OK", request: req, response: res }.to_json
        end

        def signon_failed(req, res, identifier)
            logger "Wrong responses for signon. Signon failed.", identifier
            return { status: "Failed", request: req, response: res }.to_json
        end

        def echo_success(req, res, identifier)
            logger "Echo success.", identifier
            return { status: "OK", request: req, response: res }.to_json
        end

        def echo_failed(req, res, identifier)
            logger "Wrong responses for echo. Echo failed.", identifier
            $login = false
            return { status: "Failed", request: req, response: res }.to_json
        end

        def signoff_success(req, res, identifier)
            logger "Sign off success.", identifier
            $login = false
            return { status: "OK", request: req, response: res }.to_json
        end

        def signoff_failed(req, res, identifier)
            logger "Wrong responses for signoff. Signoff failed.", identifier
            sleep 2
            return { status: "Failed", request: req, response: res }.to_json
        end
    end

    class FinancialRequest < DefaultWorker

        def general_false_response(message, request)
            message["mti"] != "0210"        || 
            message["2"]   != request["2"]  ||
            message["3"]   != request["3"]  ||
            message["11"]  != request["11"] ||
            message["32"]  != request["32"] ||
            message["37"]  != request["37"] ||
            message["47"]  != request["47"]
        end

        def false_response_for_bit48(message, request)
            if request["3"] == "400100"
                message["48"].slice( 3, 16)  != request["48"].slice( 3, 16)  ||
                message["48"].slice(19,  6)  != request["48"].slice(19,  6)  ||
                message["48"].slice(25,  3)  != request["48"].slice(25,  3)  ||
                message["48"].slice(36, 15)  != request["48"].slice(36, 15)  ||
                message["48"].slice(51, 15)  != request["48"].slice(51, 15)
            elsif request["3"] == "400101"
                false
            else
                false
            end
        end

        def is_signed_in_as_gateway(request, identifier)
            response = self.encode_to_send(request, identifier)
            message = self.decode_the(response)
            logger "Decoded res: #{message}", identifier

            if general_false_response(message, request) || false_response_for_bit48(message, request)
                get_false_response_by_djp(message, identifier)
            elsif message["39"] != "00"
                rejected_by_djp(message["39"], message["48"], message, identifier)
            else
                logger "Financial transaction success.", identifier
                { status: "OK", code: message["39"], result: message["48"], response: message, iso: response }.to_json
            end
        end

        private

        def get_false_response_by_djp(message, identifier)
            logger "Wrong response for transaction. Financtial transaction failed.", identifier
            { status: 'Failed', code: 'XX', result: 'Tanggapan dari DJP salah', response: message  }.to_json
        end

        def rejected_by_djp(response_code, bit_48, message, identifier)
            logger "Financial transaction failed, with error code (#{response_code})", identifier
            { status: 'Failed', code: response_code, result: bit_48, response: message }.to_json
        end
    end
end
