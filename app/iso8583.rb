module ISO8583

    class Decoder

        attr_accessor :iso8583

        def set_header_from_str
            return @iso8583.slice(0,12)
        end

        def set_mti_from_str
            return @iso8583.slice(0,4)
        end

        def get_bitmap_from_str
            return @iso8583.slice(0,64) if @iso8583[0] == "0"
            @iso8583.slice(0, 128)
        end

        def bitmap_value(element, mti, processing_code)
            return @iso8583.slice(0, 16) if element == "2"
            return @iso8583.slice(0,  6) if element == "3"
            return @iso8583.slice(0, 12) if element == "4"
            return @iso8583.slice(0, 10) if element == "7"
            return @iso8583.slice(0,  6) if element == "11"
            return @iso8583.slice(0,  6) if element == "12"
            return @iso8583.slice(0,  4) if element == "13"
            return @iso8583.slice(0,  4) if element == "15"
            return @iso8583.slice(0,  4) if element == "18"
            return @iso8583.slice(0,  6) if element == "32" # + kode header yaitu 04 (2 karakter)
            return @iso8583.slice(0, 12) if element == "37"
            return @iso8583.slice(0,  2) if element == "39"
            return @iso8583.slice(0, 16) if element == "41"
            return @iso8583.slice(0,  3) if element == "49"
            return @iso8583.slice(0,  9) if element == "63" # + kode header yaitu 006 (3 karakter)
            return @iso8583.slice(0,  3) if element == "70"
            case element
            when "47"
                length_get_from_header = @iso8583.slice(0,3).to_i # 3 karakter pertama adalah kode header yang menyatakan panjang value
                return @iso8583.slice(0, length_get_from_header+3)
            when "48"
                length_get_from_header = @iso8583.slice(0,3).to_i # 3 karakter pertama adalah kode header yang menyatakan panjang value
                return @iso8583.slice(0, length_get_from_header+3)
            end
        end

        def decode_this_message(string_iso)
            message = Hash.new
            
            message["header"] = self.set_header_from_str
            string_iso.slice! message["header"]
            
            message["mti"] = self.set_mti_from_str
            string_iso.slice! message["mti"]

            bitmap = self.get_bitmap_from_str
            string_iso.slice! bitmap

            bitmap.chars

            unless bitmap[0] == "1"
                (1..63).each do |element|
                    if bitmap[element] == "1"
                        sequence = (element + 1).to_s
                        message[sequence] = self.bitmap_value(sequence, message["mti"], message["3"])
                        string_iso.slice! message[sequence]
                    end
                end
            else
                (1..127).each do |element|
                    if bitmap[element] == "1"
                        sequence = (element + 1).to_s
                        message[sequence] = self.bitmap_value(sequence, message["mti"], message["3"])
                        string_iso.slice! message[sequence]
                    end
                end
            end

            #converter
            message["4"] = message["4"].to_i if message.has_key?("4")
            message["49"] = "IDR" if message["49"] == "360" && message.has_key?("49")
            message["49"] = "USD" if message["49"] == "840" && message.has_key?("49")

            return message
        end
    end 

    class Encoder

        attr_accessor :iso8583

        def setBit(kunci, nilai)
            case kunci
            when "mti"
                @iso8583 = "ISO011000017" + nilai + "0" * 64
                @mti     = nilai
            when "1"
                @iso8583[16] = "1"
                @iso8583     = @iso8583 + "0" * 64 if nilai == true
            when "2"
                @iso8583[17] = "1"
                @iso8583     = @iso8583 + nilai
            when "3"
                @iso8583[18] = "1"
                @iso8583     = @iso8583 + nilai
            when "4"
                @iso8583[19] = "1"
                nilai    = nilai.to_s.rjust(12, "0")
                @iso8583 = @iso8583 + nilai
            when "7"
                @iso8583[22] = "1"
                nilai    = Time.now.strftime "%m%d%H%M%S"
                @iso8583 = @iso8583 + nilai
            when "11"
                @iso8583[26] = "1"
                @iso8583     = @iso8583 + nilai
            when "12"
                @iso8583[27] = "1"
                nilai        = Time.now.strftime "%H%M%S"
                @iso8583     = @iso8583 + nilai
            when "13"
                @iso8583[28] = "1"
                nilai        = Time.now.strftime "%m%d"
                @iso8583     = @iso8583 + nilai
            when "15"
                @iso8583[30] = "1"
                timenow = Time.now.strftime("%H%M").to_i
                timenow < 1500 ? nilai = Time.now.strftime("%m%d") : nilai = (Time.now + (24*60*60)).strftime("%m%d")
                @iso8583 = @iso8583 + nilai
            when "18"
                @iso8583[33] = "1"
                @iso8583     = @iso8583 + nilai
            when "32"
                @iso8583[47] = "1"
                @iso8583     = @iso8583 + nilai
            when "37"
                @iso8583[52] = "1"
                @iso8583     = @iso8583 + nilai
            when "39"
                @iso8583[54] = "1"
                @iso8583     = @iso8583 + nilai
            when "41"
                @iso8583[56] = "1"
                @iso8583     = @iso8583 + nilai
            when "47"
                @iso8583[62] = "1"
                @iso8583     = @iso8583 + nilai
            when "48"
                @iso8583[63] = "1"
                @iso8583     = @iso8583 + nilai
            when "49"
                @iso8583[64] = "1"
                nilai == "IDR" ? nilai = "360" : nilai = "840"
                @iso8583 = @iso8583 + nilai
            when "63"
                @iso8583[78] = "1"
                @iso8583     = @iso8583 + nilai
            when "70"
                @iso8583[85] = "1"
                @iso8583     = @iso8583 + nilai
            end
        end
    end
end