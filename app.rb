require 'sinatra'
require 'sinatra/namespace'
require 'rest-client'
require 'rufus-scheduler'
require 'socket'
require 'net/tcp_client'
require 'securerandom'

require './initializer'

namespace '/socket/v1' do

    before do
        content_type 'application/json'
    end

    get '/signon' do
        signon = EbillingGateway::Controller.new
        signon.do_signon
    end

    get '/echo' do
        echoing = EbillingGateway::Controller.new
        echoing.do_echo
    end

    get '/signoff' do
        signoff = EbillingGateway::Controller.new
        signoff.do_signoff
    end

    post '/financial' do
        request.body.rewind
        request_payload = JSON.parse request.body.read
        fin_req = EbillingGateway::Controller.new
        fin_req.do_financial_request_as_gateway(request_payload)
    end
end

namespace '/http/v1' do

    before do
        content_type 'application/json'
    end

    put '/change/signon_duration' do 
        request.body.rewind
        request_payload = JSON.parse request.body.read
        $signon_duration = request_payload["signon_duration"]
    end

    put '/change/echo_duration' do 
        request.body.rewind
        request_payload = JSON.parse request.body.read
        $echo_duration = request_payload["echo_duration"]
    end

    put '/change/re_request' do 
        request.body.rewind
        request_payload = JSON.parse request.body.read
        $re_request = request_payload["re_request"]
    end

    put '/change/log_status' do
        request.body.rewind
        request_payload = JSON.parse request.body.read
        var = EbillingGateway::Controller.new
        var.change_log_status(request_payload)
    end

    get '/check_connection' do
        $login == true ? { code: '00' }.to_json : { code: '01' }.to_json
    end

    get '/testing/encoder' do
        encode = Tester::Controller.new
        encode.do_encode
    end

    # for receiving ISO from DJP
    get '/testing/decoder' do
        decode = Tester::Controller.new
        decode.do_decode
    end
end